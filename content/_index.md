## Automation Hub

Welcome to the Automation Hub - a service directory of operational workflow automation scripts that application developers may find handy. Additionally, we're innersourcing all of our automation projects so that developers can participate as equal stakeholders in the process.

